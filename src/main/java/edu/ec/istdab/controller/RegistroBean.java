package edu.ec.istdab.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import javax.transaction.Transactional;

import org.mindrot.jbcrypt.BCrypt;

import edu.ec.istdab.model.Persona;
import edu.ec.istdab.model.Rol;
import edu.ec.istdab.model.Usuario;
import edu.ec.istdab.service.IPersonaService;
import edu.ec.istdab.service.IRolService;

@Named
@ViewScoped
public class RegistroBean implements Serializable {
	@Inject
	private IPersonaService personaService;
	
	@Inject
	private IRolService rolService;
	
	private Persona persona;
	private Usuario usuario;
	
	@PostConstruct
	private void init() {
		this.persona = new Persona();
		this.usuario = new Usuario();
	}
	
	
	@Transactional
	public String registrar() {
		String redireccion = "";
		try {
			String clave = this.usuario.getClave();
			String claveHash = BCrypt.hashpw(clave, BCrypt.gensalt());
			this.usuario.setClave(claveHash);
			this.usuario.setPersona(persona);
			this.persona.setUsu(usuario);
			this.personaService.registrar(persona);
			
			//lista de roles para agregar al usuario
			List<Rol> roles = new ArrayList<>();
			Rol r = new Rol();
			r.setId(1);
			roles.add(r);
			
			rolService.asignar(this.usuario, roles);
			redireccion = "index?faces-redirect=true";
		} catch (Exception e) {
			
		}
		return redireccion;
	}
	
	public Persona getPersona() {
		return persona;
	}

	public void setPersona(Persona persona) {
		this.persona = persona;
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
}
