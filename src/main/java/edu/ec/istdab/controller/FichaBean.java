package edu.ec.istdab.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.event.FileUploadEvent;
import edu.ec.istdab.model.Ficha;
import edu.ec.istdab.service.IFichaService;

@Named
@ViewScoped
public class FichaBean implements Serializable {

	@Inject
	private IFichaService service;
	private Ficha persona;
	private List<Ficha> lista;



	private String tipoDialogo;


	/*
	 * public PersonaBean() { this.persona = new Persona(); //this.service = new
	 * PersonaServiceImpl(); this.listar(); }
	 */


	public Ficha getPersona() {
		return persona;
	}

	public void setPersona(Ficha persona) {
		this.persona = persona;
	}

	public List<Ficha> getLista() {
		return lista;
	}

	public void setLista(List<Ficha> lista) {
		this.lista = lista;
	}

	@PostConstruct
	public void init() {
		this.persona = new Ficha();
		this.listar();
	
	}
	
	// metodos
	public void operar(String accion) {
		try {
			if (accion.equalsIgnoreCase("R")) {
				this.service.registrar(persona);
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Persona Registrada",
						"Bienvenido"));
			} else if (accion.equalsIgnoreCase("M")) {
				this.service.modificar(persona);
				FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "Operación Exitosa", 
						"Persona Modificada"));
			}

		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	public void mostrarData(Ficha p) {
		this.persona = p;
		this.tipoDialogo = "Modificar Persona";
	}
	
	
	

	public void listar() {
		try {
			this.lista = this.service.listar();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public void limpiarControles() {
		this.persona = new Ficha();
		this.tipoDialogo = "Nueva Persona";
	}

	public String getTipoDialogo() {
		return tipoDialogo;
	}

	public void setTipoDialogo(String tipoDialogo) {
		this.tipoDialogo = tipoDialogo;
	}
	/*public void MensajeRegistro() {
        FacesContext context = FacesContext.getCurrentInstance();      
        context.addMessage(null, new FacesMessage("Persona Registrada",  "Bienvenido-Welcome") );
    }
	public void MensajeModificado() {
        FacesContext context = FacesContext.getCurrentInstance();      
        context.addMessage(null, new FacesMessage("Operacion Exitosa", "Persona Modificada") );
    }*/
}
