package edu.ec.istdab.controller;

import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;

import org.primefaces.event.FileUploadEvent;

import edu.ec.istdab.model.Persona;
import edu.ec.istdab.model.Estudiante;
import edu.ec.istdab.model.Usuario;
import edu.ec.istdab.service.IEstudianteService;

@Named
@ViewScoped
public class EstudianteBean implements Serializable {

	private Estudiante estudiante;
	private Usuario us;
	private List<Estudiante> publicaciones;
	@Inject
	private IEstudianteService service;
	
	

	@PostConstruct
	public void init() {
		this.estudiante = new Estudiante();
		this.us = (Usuario) FacesContext.getCurrentInstance().getExternalContext().getSessionMap().get("usuario");
		this.listarPublicaciones();
	}
	
	
	public void listarPublicaciones() {
		try {
			this.publicaciones = this.service.listarPublicacionesPorPublicador(this.us.getPersona());
		} catch (Exception e) {
			
		}
	}
	public void publicar() {
		try {
			Persona p = new Persona();
			p.setIdPersona(this.us.getPersona().getIdPersona());
			this.estudiante.setEstudiante(p);
			this.service.registrar(estudiante);
			this.limpiarControles();
		} catch (Exception e) {

		}
	}

	public void limpiarControles() {
		this.estudiante = new Estudiante();
	}
	public Estudiante getEstudiante() {
		return estudiante;
	}

	public void setEstudiante(Estudiante estudiante) {
		this.estudiante = estudiante;
	}

	
	public List<Estudiante> getPublicaciones() {
		return publicaciones;
	}


	public void setPublicaciones(List<Estudiante> publicaciones) {
		this.publicaciones = publicaciones;
	}


	public void handleFileUpload(FileUploadEvent event) {
		try {
			if (event.getFile() != null) {
				this.estudiante.setFoto(event.getFile().getContents());
				
			}
		} catch (Exception e) {

		}
	}
	

	

}
