package edu.ec.istdab.dao;

import java.util.List;

import javax.ejb.Local;

import edu.ec.istdab.model.Persona;
import edu.ec.istdab.model.Estudiante;

@Local
public interface IEstudianteDAO extends ICRUD<Estudiante>{
	
	List<Estudiante> listarPublicacionesPorPublicador(Persona publicador) throws Exception;
	List<Estudiante> listarPublicacionesDeSeguidores(Persona per) throws Exception;
}
