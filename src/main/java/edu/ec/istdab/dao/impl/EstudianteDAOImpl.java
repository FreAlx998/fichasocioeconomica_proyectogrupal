package edu.ec.istdab.dao.impl;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import edu.ec.istdab.dao.IEstudianteDAO;
import edu.ec.istdab.model.Persona;
import edu.ec.istdab.model.Estudiante;

@Stateless
public class EstudianteDAOImpl implements IEstudianteDAO, Serializable{

	@PersistenceContext (unitName = "blogPU")
	private EntityManager em;
	
	@Override
	public Integer registrar(Estudiante t) throws Exception {
		int rpta = 0;
		try {
			em.persist(t);
			em.flush();
			rpta = t.getId();
		} catch (Exception e) {
			rpta = 0;
		}
		return rpta;
	}

	@Override
	public Integer modificar(Estudiante t) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer eliminar(Estudiante t) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Estudiante> listar() throws Exception {
		Query q = em.createQuery("SELECT e FROM Estudiante e");
		List<Estudiante> lista = (List<Estudiante>) q.getResultList();
		return lista;
	}

	@Override
	public Estudiante listarPorId(Estudiante est) throws Exception {
		// TODO Auto-generated method stub
		Estudiante estudiante = new Estudiante();
		List<Estudiante> lista = new ArrayList<>();
		try {
			Query query = em.createQuery("FROM Estudiante e WHERE e.id=?1");
			query.setParameter(1, est.getId());
			lista = (List<Estudiante>) query.getResultList();
			if (lista != null && !lista.isEmpty()) {
				estudiante = lista.get(0);
			}
		} catch (Exception e) {
			throw e;
		}
		return estudiante;
	}
	@Override
	public List<Estudiante> listarPublicacionesPorPublicador(Persona publicador) throws Exception {
		List<Estudiante> lista = new ArrayList();
		try {
			Query query = em.createQuery("FROM Estudiante p WHERE p.estudiante.idPersona =?1");
			query.setParameter(1, publicador.getIdPersona());
			lista = (List<Estudiante>) query.getResultList();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		}
		return lista;
	}

	@Override
	public List<Estudiante> listarPublicacionesDeSeguidores(Persona per)  {
		
		return null;
	}

	

}
