package edu.ec.istdab.dao.impl;

import java.io.Serializable;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import edu.ec.istdab.dao.IFichaDAO;
import edu.ec.istdab.model.Ficha;

@Stateless
public class FichaDAOImpl implements IFichaDAO, Serializable{

	@PersistenceContext (unitName = "blogPU")
	private EntityManager em;
	
	@Override
	public Integer registrar(Ficha t) throws Exception {
		em.persist(t);
		return t.getId();
	}

	@Override
	public Integer modificar(Ficha t) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer eliminar(Ficha t) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Ficha> listar() throws Exception {
		Query q = em.createQuery("SELECT f FROM Ficha f");
		List<Ficha> lista = (List<Ficha>) q.getResultList();
		return lista;
	}

	@Override
	public Ficha listarPorId(Ficha est) throws Exception {
		// TODO Auto-generated method stub
	
		return null;
	}
	

}
