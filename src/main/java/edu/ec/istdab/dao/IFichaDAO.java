package edu.ec.istdab.dao;

import javax.ejb.Local;
import edu.ec.istdab.model.Ficha;

@Local
public interface IFichaDAO extends ICRUD<Ficha>{

}
