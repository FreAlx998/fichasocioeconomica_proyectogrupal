package edu.ec.istdab.dao;

import javax.ejb.Local;

import edu.ec.istdab.model.Persona;

@Local
public interface IPersonaDAO extends ICRUD<Persona>{

}
