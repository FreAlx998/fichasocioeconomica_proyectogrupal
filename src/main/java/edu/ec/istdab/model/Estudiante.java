package edu.ec.istdab.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "estudiante")
public class Estudiante implements Serializable{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	

	@ManyToOne
	@JoinColumn(name = "id_estudiante", nullable = false)
	private Persona estudiante;
	
	
	@Column(name = "nombre", nullable = false, length = 250)
	private String nombre;
	
	
	@Column(name = "fecha", nullable = false)
	private LocalDateTime fecha;
	
	public LocalDateTime getFecha() {
		return fecha;
	}

	public void setFecha(LocalDateTime fecha) {
		this.fecha = fecha;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name = "apellidos", nullable = false, length = 50)
	private String apellidos;
	
	@Column(name = "sexo", nullable = false, length = 1)
	private String sexo;
	
	@Column(name ="pais", nullable = false, length = 30)
	private String pais;
	
	@Column(name = "direccion", nullable = false, length = 150)
	private String direccion;

	@Column(name = "correo", nullable = false, length = 150)
	private String correo;
	
	@Column(name = "edad", nullable = false, length = 150)
	private String edad;
	
	
	@Column(name = "familiares", nullable = false, length = 150)
	private String familiares;

	public Persona getEstudiante() {
		return estudiante;
	}

	public void setEstudiante(Persona estudiante) {
		this.estudiante = estudiante;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public String getApellidos() {
		return apellidos;
	}

	public void setApellidos(String apellidos) {
		this.apellidos = apellidos;
	}

	public String getSexo() {
		return sexo;
	}

	public void setSexo(String sexo) {
		this.sexo = sexo;
	}

	public String getPais() {
		return pais;
	}

	public void setPais(String pais) {
		this.pais = pais;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getEdad() {
		return edad;
	}

	public void setEdad(String edad) {
		this.edad = edad;
	}

	public String getFamiliares() {
		return familiares;
	}

	public void setFamiliares(String familiares) {
		this.familiares = familiares;
	}

	
	@Column(name = "foto", nullable = true)
	private byte[] foto;


	public byte[] getFoto() {
		return foto;
	}

	public void setFoto(byte[] foto) {
		this.foto = foto;
	}
	
	
}
