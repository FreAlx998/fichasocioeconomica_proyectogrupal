package edu.ec.istdab.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MapsId;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "ficha")
public class Ficha implements Serializable{
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	

	@Column(name = "padres", nullable = false, length = 250)
	private String padres;
	
	
	@Column(name = "fecha", nullable = false)
	private LocalDateTime fecha;
	
	public LocalDateTime getFecha() {
		return fecha;
	}

	public void setFecha(LocalDateTime fecha) {
		this.fecha = fecha;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}
	
	@Column(name = "Casa", nullable = false, length = 50)
	private String Casa;
	
	@Column(name = "Ingresos", nullable = false, length = 25)
	private String Ingresos;
	
	@Column(name ="Enfermedad", nullable = false, length = 30)
	private String Enfermedad;
	
	@Column(name = "Bienes", nullable = false, length = 1)
	private String Bienes;

	@Column(name = "Estrangero", nullable = false, length = 150)
	private String Estrangero;

	public String getPadres() {
		return padres;
	}

	public void setPadres(String padres) {
		this.padres = padres;
	}

	public String getCasa() {
		return Casa;
	}

	public void setCasa(String casa) {
		Casa = casa;
	}

	public String getIngresos() {
		return Ingresos;
	}

	public void setIngresos(String ingresos) {
		Ingresos = ingresos;
	}

	public String getEnfermedad() {
		return Enfermedad;
	}

	public void setEnfermedad(String enfermedad) {
		Enfermedad = enfermedad;
	}

	public String getBienes() {
		return Bienes;
	}

	public void setBienes(String bienes) {
		Bienes = bienes;
	}

	public String getEstrangero() {
		return Estrangero;
	}

	public void setEstrangero(String estrangero) {
		Estrangero = estrangero;
	}
	
	

	
	
}
