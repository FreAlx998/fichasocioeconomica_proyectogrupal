package edu.ec.istdab.service;

import java.util.List;

import edu.ec.istdab.model.Persona;
import edu.ec.istdab.model.Estudiante;

public interface IEstudianteService extends IService<Estudiante>{
	List<Estudiante> listarPublicacionesPorPublicador(Persona publicador) throws Exception;
	List<Estudiante> listarPublicacionesDeSeguidores(Persona per) throws Exception;
	
}
