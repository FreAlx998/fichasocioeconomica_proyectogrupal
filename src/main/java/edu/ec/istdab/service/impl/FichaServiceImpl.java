package edu.ec.istdab.service.impl;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import javax.ejb.EJB;
import javax.inject.Named;

import edu.ec.istdab.dao.IEstudianteDAO;
import edu.ec.istdab.dao.IFichaDAO;
import edu.ec.istdab.model.Persona;
import edu.ec.istdab.model.Estudiante;
import edu.ec.istdab.model.Ficha;
import edu.ec.istdab.service.IEstudianteService;
import edu.ec.istdab.service.IFichaService;

@Named
public class FichaServiceImpl implements IFichaService, Serializable{

	
	@EJB
	private IFichaDAO dao;
	
	@Override
	public Integer registrar(Ficha t) throws Exception {
		// TODO Auto-generated method stub
		t.setFecha(LocalDateTime.now());
		int rpta = dao.registrar(t);
		//logica de retorno
		return rpta > 0 ? 1 : 0;
	}

	@Override
	public Integer modificar(Ficha t) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Integer eliminar(Ficha t) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Ficha> listar() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Ficha listarPorId(Ficha t) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}


}
