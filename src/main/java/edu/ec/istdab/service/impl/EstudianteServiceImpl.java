package edu.ec.istdab.service.impl;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import javax.ejb.EJB;
import javax.inject.Named;

import edu.ec.istdab.dao.IEstudianteDAO;
import edu.ec.istdab.model.Persona;
import edu.ec.istdab.model.Estudiante;
import edu.ec.istdab.service.IEstudianteService;

@Named
public class EstudianteServiceImpl implements IEstudianteService, Serializable{
	
	@EJB
	private IEstudianteDAO dao;
	
	@Override
	public Integer registrar(Estudiante estudiante) throws Exception {
		//Buen inicio de semana @Analisis, de #Sistemas
		//Buen inicio de semana @Analisis de #Sistemas
		estudiante.setFecha(LocalDateTime.now());
		int rpta = dao.registrar(estudiante);
		//logica de retorno
		return rpta > 0 ? 1 : 0;
	}

	@Override
	public Integer modificar(Estudiante t) throws Exception {
		return null;
	}

	@Override
	public Integer eliminar(Estudiante t) throws Exception {
		return null;
	}

	@Override
	public List<Estudiante> listar() throws Exception {
		return dao.listar();
	}

	@Override
	public Estudiante listarPorId(Estudiante est) throws Exception {
		return dao.listarPorId(est);
	}

	@Override
	public List<Estudiante> listarPublicacionesPorPublicador(Persona publicador) throws Exception {
		return dao.listarPublicacionesPorPublicador(publicador);
	}

	@Override
	public List<Estudiante> listarPublicacionesDeSeguidores(Persona per) throws Exception{
		
		return dao.listarPublicacionesDeSeguidores(per);
	}

}
