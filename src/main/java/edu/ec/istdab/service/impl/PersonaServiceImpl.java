package edu.ec.istdab.service.impl;

import java.io.Serializable;
import java.util.List;

import javax.ejb.EJB;
import javax.inject.Named;

import edu.ec.istdab.dao.IPersonaDAO;
import edu.ec.istdab.model.Persona;
import edu.ec.istdab.service.IPersonaService;

@Named
public class PersonaServiceImpl implements IPersonaService, Serializable{
	
	@EJB
	private IPersonaDAO dao;
	
	@Override
	public Integer registrar(Persona t) throws Exception {
		return dao.registrar(t);
	}

	@Override
	public Integer modificar(Persona t) throws Exception {
		return dao.modificar(t);
	}

	@Override
	public Integer eliminar(Persona t) throws Exception {
		return dao.eliminar(t);
	}

	@Override
	public List<Persona> listar() throws Exception {
		return dao.listar();
	}

	@Override
	public Persona listarPorId(Persona t) throws Exception {
		return dao.listarPorId(t);
	}

}
